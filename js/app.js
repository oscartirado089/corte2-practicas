/* DECLARAR VARIABLES */

let historial = [];

const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener("click", function () {
    //OBTENER LOS DATOS DE LOS INPUT
    let valorAuto = document.getElementById('ValorAuto').value;
    let pInicial = document.getElementById('Porcentaje').value;
    let plazos = document.getElementById('Plazos').value;
    let parrafo = document.getElementById('pa');

    //HACER LOS CALCULOS
    let pagoInicial = valorAuto * (pInicial) / 100;
    let totalfin = valorAuto - pagoInicial;
    let pagoMensual = totalfin / plazos;

    //MOSTRAR LOS DATOS
    document.getElementById('pagoInicial').value = pagoInicial;
    document.getElementById('totalfin').value = totalfin;
    document.getElementById('pagoMensual').value = pagoMensual;
})

function arribaMouse() {
    let parrafo = document.getElementById('pa');
    parrafo.style.color = "#FF00FF";
    parrafo.style.fontSize = '25px';
    parrafo.style.textAlign = 'justify';
}

function salirMouse() {
    let parrafo = document.getElementById('pa');
    parrafo.style.color = "red";
    parrafo.style.fontSize = '17px';
    parrafo.style.textAlign = 'left';
}

function limpiar() {
    let parrafo = document.getElementById('pa');
    texto = parrafo.innerHTML;
    parrafo.innerHTML = "";
}

function calc() {
    // OBTENER LOS DATOS
    let valorAuto = parseFloat(document.getElementById('ValorAuto').value);
    let pInicial = parseFloat(document.getElementById('Porcentaje').value);
    let plazos = parseFloat(document.getElementById('Plazos').value);

    // CALCULOS
    let pagoInicial = valorAuto * (pInicial) / 100;
    let totalfin = valorAuto - pagoInicial;
    let pagoMensual = totalfin / plazos;

    let cotizacionActual = {
        valorAuto: valorAuto,
        porcentajeInicial: pInicial,
        plazos: plazos,
        pagoInicial: pagoInicial.toFixed(2),
        totalFinanciar: totalfin.toFixed(2),
        pagoMensual: pagoMensual.toFixed(2)
    };

    historial.push(cotizacionActual);

    mostrarHistorial();
}

function mostrarHistorial() {
    let parrafo = document.getElementById('pa');

    parrafo.innerHTML = "";

    historial.forEach((cotizacion, index) => {
        let nuevoParrafo = document.createElement('p');
        nuevoParrafo.textContent = `Cotización ${index + 1}: 
            Valor del Auto = $${cotizacion.valorAuto}, 
            Porcentaje Inicial = ${cotizacion.porcentajeInicial}%, 
            Plazos = ${cotizacion.plazos} Meses, 
            Pago Inicial = $${cotizacion.pagoInicial}, 
            Total a Financiar = $${cotizacion.totalFinanciar}, 
            Pago Mensual = $${cotizacion.pagoMensual}`;

        parrafo.appendChild(nuevoParrafo);
    });
}
