document.addEventListener("DOMContentLoaded", function () {
    const form = document.getElementById("imcForm");
    form.addEventListener("submit", function (e) {
        e.preventDefault();

        const altura = parseFloat(document.getElementById("altura").value);
        const peso = parseFloat(document.getElementById("peso").value);
        const edad = parseInt(document.getElementById("edad").value);

        if (!isNaN(altura) && !isNaN(peso) && !isNaN(edad)) {
            const imc = peso / ((altura / 100) * (altura / 100));
            const resultado = document.getElementById("resultado");
            resultado.innerHTML = `Tu IMC es: ${imc.toFixed(2)}`;

            let nivel;
            let imagen;
            if (imc < 18.5) {
                nivel = "Peso por debajo de lo normal";
                imagen = "/img/01.png";
            } else if (imc >= 18.5 && imc < 25) {
                nivel = "Peso saludable";
                imagen = "/img/02.png";
            } else if (imc >= 25 && imc < 30) {
                nivel = "Sobrepeso";
                imagen = "/img/03.png";
            } else if (imc >= 30 && imc < 35) {
                nivel = "Obesidad Tipo I";
                imagen = "/img/04.png";
            } else if (imc >= 35 && imc < 40) {
                nivel = "Obesidad Tipo II";
                imagen = "/img/05.png";
            } else {
                nivel = "Obesidad Tipo III";
                imagen = "/img/06.png";
            }

            resultado.innerHTML += `<br>Nivel: ${nivel}`;

            const imagenResultado = document.getElementById("imagenResultado");
            imagenResultado.style.display = "block"; // Mostrar la imagen
            imagenResultado.innerHTML = `<img src="${imagen}" alt="Nivel de IMC">`;

            let calorias = 0;
            if (edad >= 10 && edad < 18) {
                calorias = (15.057 * peso) + 692.2; // Hombres
            } else if (edad >= 18 && edad < 30) {
                calorias = (14.818 * peso) + 486.6; // Mujeres
            } else if (edad >= 30 && edad < 60) {
                calorias = (11.472 * peso) + 873.1; // Hombres
            } else {
                calorias = (9.082 * peso) + 658.5; // Mujeres
            }

            resultado.innerHTML += `<br>Calorías al día: ${calorias.toFixed(2)}`;
        }
    });
});
